<?php 

namespace Onemineral\PMS\SDK;

class PMSProxy
{
	public static function activity(): Resources\Activity
	{
		return new Resources\Activity;
	}


	public static function user(): Resources\User
	{
		return new Resources\User;
	}


	public static function tenant(): Resources\Tenant
	{
		return new Resources\Tenant;
	}


	public static function account(): Resources\Account
	{
		return new Resources\Account;
	}


	public static function partner_account(): Resources\PartnerAccount
	{
		return new Resources\PartnerAccount;
	}


	public static function guest_account(): Resources\GuestAccount
	{
		return new Resources\GuestAccount;
	}


	public static function profile(): Resources\Profile
	{
		return new Resources\Profile;
	}


	public static function profile_settings(): Resources\ProfileSettings
	{
		return new Resources\ProfileSettings;
	}


	public static function mutation(): Resources\Mutation
	{
		return new Resources\Mutation;
	}


	public static function legal_entity(): Resources\LegalEntity
	{
		return new Resources\LegalEntity;
	}


	public static function tenant_policy(): Resources\TenantPolicy
	{
		return new Resources\TenantPolicy;
	}


	public static function rental_agreement(): Resources\RentalAgreement
	{
		return new Resources\RentalAgreement;
	}


	public static function tenant_settings(): Resources\TenantSettings
	{
		return new Resources\TenantSettings;
	}


	public static function tracking(): Resources\Tracking
	{
		return new Resources\Tracking;
	}


	public static function settings(): Resources\Settings
	{
		return new Resources\Settings;
	}


	public static function data(): Resources\Data
	{
		return new Resources\Data;
	}


	public static function search(): Resources\Search
	{
		return new Resources\Search;
	}


	public static function currency(): Resources\Currency
	{
		return new Resources\Currency;
	}


	public static function currency_conversion_rate(): Resources\ConversionRate
	{
		return new Resources\ConversionRate;
	}


	public static function country(): Resources\Country
	{
		return new Resources\Country;
	}


	public static function language(): Resources\Language
	{
		return new Resources\Language;
	}


	public static function property(): Resources\Property
	{
		return new Resources\Property;
	}


	public static function property_policy(): Resources\PropertyPolicy
	{
		return new Resources\PropertyPolicy;
	}


	public static function property_rates_settings(): Resources\PropertyRatesSettings
	{
		return new Resources\PropertyRatesSettings;
	}


	public static function property_host_revenue_share(): Resources\PropertyHostRevenueShare
	{
		return new Resources\PropertyHostRevenueShare;
	}


	public static function amenity(): Resources\Amenity
	{
		return new Resources\Amenity;
	}


	public static function amenity_group(): Resources\AmenityGroup
	{
		return new Resources\AmenityGroup;
	}


	public static function property_type(): Resources\PropertyType
	{
		return new Resources\PropertyType;
	}


	public static function location(): Resources\Location
	{
		return new Resources\Location;
	}


	public static function property_room(): Resources\PropertyRoom
	{
		return new Resources\PropertyRoom;
	}


	public static function property_room_bed(): Resources\PropertyRoomBed
	{
		return new Resources\PropertyRoomBed;
	}


	public static function bed_type(): Resources\BedType
	{
		return new Resources\BedType;
	}


	public static function region(): Resources\Region
	{
		return new Resources\Region;
	}


	public static function review(): Resources\Review
	{
		return new Resources\Review;
	}


	public static function quote(): Resources\Quote
	{
		return new Resources\Quote;
	}


	public static function image(): Resources\Image
	{
		return new Resources\Image;
	}


	public static function seo(): Resources\Seo
	{
		return new Resources\Seo;
	}


	public static function tag(): Resources\Tag
	{
		return new Resources\Tag;
	}


	public static function collection(): Resources\Collection
	{
		return new Resources\Collection;
	}


	public static function discount(): Resources\Discount
	{
		return new Resources\Discount;
	}


	public static function applicable_discount(): Resources\ApplicableDiscount
	{
		return new Resources\ApplicableDiscount;
	}


	public static function payment_schedule(): Resources\PaymentSchedule
	{
		return new Resources\PaymentSchedule;
	}


	public static function payment_schedule_step(): Resources\PaymentScheduleStep
	{
		return new Resources\PaymentScheduleStep;
	}


	public static function payment_gateway(): Resources\PaymentGateway
	{
		return new Resources\PaymentGateway;
	}


	public static function credit_card(): Resources\CreditCard
	{
		return new Resources\CreditCard;
	}


	public static function payment_method(): Resources\PaymentMethod
	{
		return new Resources\PaymentMethod;
	}


	public static function payment(): Resources\Payment
	{
		return new Resources\Payment;
	}


	public static function security_deposit(): Resources\SecurityDeposit
	{
		return new Resources\SecurityDeposit;
	}


	public static function rates_availability(): Resources\RatesAvailability
	{
		return new Resources\RatesAvailability;
	}


	public static function property_ical(): Resources\PropertyIcal
	{
		return new Resources\PropertyIcal;
	}


	public static function availability(): Resources\Availability
	{
		return new Resources\Availability;
	}


	public static function availability_status(): Resources\AvailabilityStatus
	{
		return new Resources\AvailabilityStatus;
	}


	public static function property_los_season(): Resources\PropertyLosSeason
	{
		return new Resources\PropertyLosSeason;
	}


	public static function property_los_season_discount(): Resources\PropertyLosSeasonDiscount
	{
		return new Resources\PropertyLosSeasonDiscount;
	}


	public static function fee_type(): Resources\FeeType
	{
		return new Resources\FeeType;
	}


	public static function property_fee(): Resources\PropertyFee
	{
		return new Resources\PropertyFee;
	}


	public static function unavailability(): Resources\Unavailability
	{
		return new Resources\Unavailability;
	}


	public static function availability_details(): Resources\AvailabilityDetails
	{
		return new Resources\AvailabilityDetails;
	}


	public static function rates_availability_range(): Resources\RatesAvailabilityRange
	{
		return new Resources\RatesAvailabilityRange;
	}


	public static function los_template(): Resources\LosTemplate
	{
		return new Resources\LosTemplate;
	}


	public static function property_occupancy_season(): Resources\PropertyOccupancySeason
	{
		return new Resources\PropertyOccupancySeason;
	}


	public static function property_occupancy_rate(): Resources\PropertyOccupancyRate
	{
		return new Resources\PropertyOccupancyRate;
	}


	public static function tax(): Resources\Tax
	{
		return new Resources\Tax;
	}


	public static function tax_class(): Resources\TaxClass
	{
		return new Resources\TaxClass;
	}


	public static function calculated_tax(): Resources\CalculatedTax
	{
		return new Resources\CalculatedTax;
	}


	public static function personal_access_token(): Resources\PersonalAccessToken
	{
		return new Resources\PersonalAccessToken;
	}


	public static function identity(): Resources\Identity
	{
		return new Resources\Identity;
	}


	public static function booking(): Resources\Booking
	{
		return new Resources\Booking;
	}


	public static function booking_product(): Resources\BookingProduct
	{
		return new Resources\BookingProduct;
	}


	public static function booking_product_line_item(): Resources\BookingProductLineItem
	{
		return new Resources\BookingProductLineItem;
	}


	public static function booking_payment_schedule_step(): Resources\BookingPaymentScheduleStep
	{
		return new Resources\BookingPaymentScheduleStep;
	}


	public static function booking_ta_commission(): Resources\BookingTaCommission
	{
		return new Resources\BookingTaCommission;
	}


	public static function booking_host_revenue_share(): Resources\BookingHostRevenueShare
	{
		return new Resources\BookingHostRevenueShare;
	}


	public static function import_bulk_booking(): Resources\ImportBulkBooking
	{
		return new Resources\ImportBulkBooking;
	}


	public static function booking_guest(): Resources\BookingGuest
	{
		return new Resources\BookingGuest;
	}


	public static function notification(): Resources\Notification
	{
		return new Resources\Notification;
	}


	public static function email_configuration(): Resources\EmailConfiguration
	{
		return new Resources\EmailConfiguration;
	}


	public static function notification_layout(): Resources\NotificationLayout
	{
		return new Resources\NotificationLayout;
	}


	public static function notification_template(): Resources\NotificationTemplate
	{
		return new Resources\NotificationTemplate;
	}


	public static function message(): Resources\Message
	{
		return new Resources\Message;
	}


	public static function channel(): Resources\Channel
	{
		return new Resources\Channel;
	}


	public static function channel_property(): Resources\ChannelProperty
	{
		return new Resources\ChannelProperty;
	}


	public static function channel_manager_sync(): Resources\ChannelManagerSync
	{
		return new Resources\ChannelManagerSync;
	}


	public static function salesforce(): Resources\Salesforce
	{
		return new Resources\Salesforce;
	}


	public static function analytics(): Resources\Analytics
	{
		return new Resources\Analytics;
	}


	public static function report(): Resources\Report
	{
		return new Resources\Report;
	}


	public static function health_score(): Resources\HealthScore
	{
		return new Resources\HealthScore;
	}


	public static function service(): Resources\Service
	{
		return new Resources\Service;
	}


	public static function service_type(): Resources\ServiceType
	{
		return new Resources\ServiceType;
	}


	public static function service_request(): Resources\ServiceRequest
	{
		return new Resources\ServiceRequest;
	}


	public static function stay(): Resources\Stay
	{
		return new Resources\Stay;
	}


	public static function supplier_type(): Resources\SupplierType
	{
		return new Resources\SupplierType;
	}


	public static function airbnb(): Resources\Airbnb
	{
		return new Resources\Airbnb;
	}


	public static function airbnb_property(): Resources\AirbnbProperty
	{
		return new Resources\AirbnbProperty;
	}


	public static function homeaway(): Resources\Homeaway
	{
		return new Resources\Homeaway;
	}


	public static function homeaway_property(): Resources\HomeawayProperty
	{
		return new Resources\HomeawayProperty;
	}


	public static function api_partner(): Resources\ApiPartner
	{
		return new Resources\ApiPartner;
	}


	public static function api_partner_auth(): Resources\ApiPartnerAuth
	{
		return new Resources\ApiPartnerAuth;
	}


	public static function booking_com(): Resources\BookingCom
	{
		return new Resources\BookingCom;
	}


	public static function booking_com_property(): Resources\BookingComProperty
	{
		return new Resources\BookingComProperty;
	}


	public static function booking_com_legal_entity(): Resources\BookingComLegalEntity
	{
		return new Resources\BookingComLegalEntity;
	}


	public static function dtravel(): Resources\Dtravel
	{
		return new Resources\Dtravel;
	}


	public static function dtravel_property(): Resources\DtravelProperty
	{
		return new Resources\DtravelProperty;
	}


	public static function breezeway(): Resources\Breezeway
	{
		return new Resources\Breezeway;
	}


	public static function breezeway_property(): Resources\BreezewayProperty
	{
		return new Resources\BreezewayProperty;
	}


	public static function rentals_united(): Resources\RentalsUnited
	{
		return new Resources\RentalsUnited;
	}


	public static function rentals_united_property(): Resources\RentalsUnitedProperty
	{
		return new Resources\RentalsUnitedProperty;
	}


	public static function google_vr(): Resources\GoogleVR
	{
		return new Resources\GoogleVR;
	}


	public static function google_vr_property(): Resources\GoogleVRProperty
	{
		return new Resources\GoogleVRProperty;
	}


	public static function superhog(): Resources\Superhog
	{
		return new Resources\Superhog;
	}


	public static function import_supplier(): Resources\ImportSupplier
	{
		return new Resources\ImportSupplier;
	}


	public static function workflow(): Resources\Workflow
	{
		return new Resources\Workflow;
	}


	public static function workflow_schedule(): Resources\WorkflowSchedule
	{
		return new Resources\WorkflowSchedule;
	}
}
