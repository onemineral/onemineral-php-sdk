<?php

namespace Onemineral\PMS\SDK;

class Conditions
{

    protected array $conditions = [];

    protected array $operations = [
        '=' => 'eq',
        '!=' => 'not_eq',
        '<>' => 'not_eq',
        '>' => 'gt',
        '>=' => 'gte',
        '<' => 'lt',
        '<=' => 'lte'
    ];

    /**
     * @return Conditions
     */
    public static function factory(): Conditions
    {
        return new Conditions();
    }

    /**
     * @param string|Conditions $field
     * @param string|null       $operation
     * @param mixed|null        $value
     * @param string            $boolean
     * @return Conditions
     */
    public function where(
         string|Conditions $field,
         ?string $operation = null,
         mixed $value = null,
         string $boolean = 'and'
    ): static
    {
        if($field instanceof Conditions) {
            $this->conditions[] = [
                'boolean' => $boolean,
                'operation' => $field
            ];
        } else {
            $this->conditions[] = [
                'boolean' => $boolean,
                'operation' => [
                    'field' => $field,
                    $this->getOperation($operation) => $value
                ]
            ];
        }

        return $this;
    }

    /**
     * @param string $operation
     * @return string
     */
    protected function getOperation(string $operation): string
    {
        $operation = trim($operation);
        return $this->operations[$operation] ?? $operation;
    }

    /**
     * @param string|Conditions $field
     * @param string|null       $operation
     * @param mixed|null        $value
     * @return $this
     */
    public function orWhere(string|Conditions $field, ?string $operation = null, mixed $value = null): static
    {
        return $this->where($field, $operation, $value, 'or');
    }

    /**
     * @return array
     */
    public function getConditions(): array
    {
        $conditions = [];

        foreach($this->conditions as $condition) {
            if($condition['operation'] instanceof Conditions) {
                $conditions = array_merge($conditions, $condition['operation']->getConditions());
            } else {
                $conditions[] = $condition['operation'];
            }
        }

        return $conditions;
    }

    /**
     * @param int $index
     * @return array
     */
    public function getConditionsLogic(int $index = 0): array
    {
        $logic = '';
        foreach($this->conditions as $key => $condition) {
            if($key !== 0) {
                $logic .= ' ' . $condition['boolean'] . ' ';
            }

            if($condition['operation'] instanceof Conditions) {
                $result = $condition['operation']->getConditionsLogic($index);
                $logic .= '(' . $result['logic'] .')';
                $index  = $result['index'];
            } else {
                $index ++;
                $logic .= (string) $index;
            }
        }

        return [
            'logic' => $logic,
            'index' => $index
        ];
    }

    /**
     * @param bool $forAggregations
     * @return array
     */
    public function toArray(bool $forAggregations = false): array
    {
        return [
            ($forAggregations ? 'aggregate_conditions' : 'conditions') => $this->getConditions(),
            ($forAggregations ? 'aggregate_conditions_logic' : 'conditions_logic') => $this->getConditionsLogic()['logic']
        ];
    }

}