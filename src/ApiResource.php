<?php

namespace Onemineral\PMS\SDK;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Database\Eloquent\Concerns\HasAttributes;
use Illuminate\Support\Arr;

abstract class ApiResource implements Jsonable, \JsonSerializable, Arrayable, \ArrayAccess
{
    use HasAttributes;

    protected string $path;

    /**
     * ApiResource constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->attributes = $attributes;
	    $this->dateFormat = 'Y-m-d H:i:s';
    }

    /**
     * @param $key
     * @return mixed
     */
    public function __get($key)
    {
        if (array_key_exists($key, $this->attributes) &&
            array_key_exists($key, $this->casts)) {
            return $this->castAttributeValue($key);
        }

        return Arr::get($this->attributes, $key);
    }

    /**
     * @return bool
     */
    public function usesTimestamps(): bool
    {
        return false;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function castAttributeValue($key)
    {
        $cast  = Arr::get($this->casts, $key);
        $value = Arr::get($this->attributes, $key);

        if($value === null) {
            return null;
        }

        if(class_exists($cast)) {
            if(is_array($value) && !$value) {
                // is a relation with no results
                return collect([]);
            }

            if(isset($value[0])) {
                return collect($value)->map(fn($item) => new $cast($item));
            }

            return new $cast($value);
        }

        return $this->castAttribute($key, $value);
    }

    /**
     * @return null
     */
    protected function getIncrementing()
    {
        return null;
    }

    /**
     * @param int $options
     * @return false|string
     */
    public function toJson($options = 0)
    {
        return json_encode($this->jsonSerialize());
    }

    /**
     * @return array|mixed
     */
    public function jsonSerialize(): mixed
    {
        return $this->toArray();
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->attributes;
    }

    public function offsetExists($offset): bool
    {
        return array_key_exists($offset, $this->attributes);
    }

    public function offsetGet($offset): mixed
    {
        return $this->__get($offset);
    }

    public function offsetSet($offset, $value): void
    {
        $this->setAttribute($offset, $value);
    }

    public function offsetUnset($offset): void
    {
    }


}
