<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property NotificationTemplate|null $notification_template
 * @property mixed|null $receiver
 * @property EmailConfiguration|null $email_configuration
 * @property string|null $status
 * @property string|null $type
 * @property string|null $provider
 * @property string|null $subject
 * @property string|null $body
 * @property array $to
 * @property array $from
 * @property string|null $direction
 * @property string|null $external_message_id
 * @property mixed|null $resource
 * @property int|null $bounced
 * @property int|null $delivered
 * @property int|null $opened
 * @property int|null $read
 * @property int|null $clicked
 * @property array $metadata
 * @property User|null $sent_by
 * @property Workflow|null $workflow
 * @property \Carbon\Carbon|null $sent_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class Message extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'message';

	protected $casts = [
		'notification_template' => '\Onemineral\PMS\SDK\Resources\NotificationTemplate',
		'email_configuration' => '\Onemineral\PMS\SDK\Resources\EmailConfiguration',
		'sent_by' => '\Onemineral\PMS\SDK\Resources\User',
		'workflow' => '\Onemineral\PMS\SDK\Resources\Workflow',
		'sent_at' => 'date',
		'created_at' => 'date',
		'updated_at' => 'date',
	];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function query(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/query", "\Onemineral\PMS\SDK\Resources\Message");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function fetch(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/fetch", "\Onemineral\PMS\SDK\Resources\Message");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function send(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/send", "\Onemineral\PMS\SDK\Resources\Message");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function get_threads(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/get-threads", "\Onemineral\PMS\SDK\Resources\Message");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function get_thread_messages(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/get-thread-messages", "\Onemineral\PMS\SDK\Resources\Message");
	}
}
