<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property Currency[]|\Illuminate\Support\Collection $currencies
 * @property Language[]|\Illuminate\Support\Collection $languages
 * @property Tenant|null $tenant
 * @property Profile|null $profile
 * @property string|null $redirect_to
 * @property array $schema
 */
class Settings extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'settings';

	protected $casts = [
		'currencies' => '\Onemineral\PMS\SDK\Resources\Currency',
		'languages' => '\Onemineral\PMS\SDK\Resources\Language',
		'tenant' => '\Onemineral\PMS\SDK\Resources\Tenant',
		'profile' => '\Onemineral\PMS\SDK\Resources\Profile',
	];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function fetch(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/fetch", "\Onemineral\PMS\SDK\Resources\Settings");
	}
}
