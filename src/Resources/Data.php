<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property array $data
 */
class Data extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'data';
	protected $casts = [];
}
