<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property string|null $name
 * @property string|null $email
 * @property string|null $role
 * @property bool $subscribe_financials
 * @property bool $subscribe_reservations
 * @property bool $subscribe_inventory
 * @property bool $subscribe_distribution
 * @property ProfileSettings|null $settings
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class Profile extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'profile';

	protected $casts = [
		'subscribe_financials' => 'bool',
		'subscribe_reservations' => 'bool',
		'subscribe_inventory' => 'bool',
		'subscribe_distribution' => 'bool',
		'settings' => '\Onemineral\PMS\SDK\Resources\ProfileSettings',
		'created_at' => 'date',
		'updated_at' => 'date',
	];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function fetch(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/fetch", "\Onemineral\PMS\SDK\Resources\Profile");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update", "\Onemineral\PMS\SDK\Resources\Profile");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function change_password(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/change-password", "\Onemineral\PMS\SDK\Resources\Profile");
	}
}
