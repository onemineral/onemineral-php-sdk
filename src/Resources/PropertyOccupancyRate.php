<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int|null $occupancy
 * @property int|null $bedrooms
 * @property array $rates
 */
class PropertyOccupancyRate extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'property-occupancy-rate';
	protected $casts = [];
}
