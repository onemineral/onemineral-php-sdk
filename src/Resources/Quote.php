<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property array $search_criteria
 * @property Property|null $property
 * @property Booking|null $booking
 * @property Currency|null $currency
 * @property PartnerAccount|null $booker
 * @property BookingProduct[]|\Illuminate\Support\Collection $products
 * @property mixed|null $total
 * @property mixed|null $total_pretax
 * @property mixed|null $total_taxes
 * @property mixed|null $total_accommodation
 * @property mixed|null $total_accommodation_pretax
 * @property mixed|null $total_fees
 * @property mixed|null $total_fees_pretax
 * @property mixed|null $total_before_discount
 * @property mixed|null $total_discount
 * @property mixed|null $security_deposit
 * @property Currency|null $security_deposit_currency
 * @property BookingTaCommission|null $commission
 * @property string|null $quote_id
 * @property string|null $book_url
 * @property array $exceptions
 * @property array $warnings
 */
class Quote extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'quote';

	protected $casts = [
		'property' => '\Onemineral\PMS\SDK\Resources\Property',
		'booking' => '\Onemineral\PMS\SDK\Resources\Booking',
		'currency' => '\Onemineral\PMS\SDK\Resources\Currency',
		'booker' => '\Onemineral\PMS\SDK\Resources\PartnerAccount',
		'products' => '\Onemineral\PMS\SDK\Resources\BookingProduct',
		'security_deposit_currency' => '\Onemineral\PMS\SDK\Resources\Currency',
		'commission' => '\Onemineral\PMS\SDK\Resources\BookingTaCommission',
	];
}
