<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property Property|null $property
 * @property PartnerAccount|null $booker
 * @property Currency|null $currency
 * @property string|null $type
 * @property \Carbon\Carbon|null $checkin
 * @property \Carbon\Carbon|null $checkout
 * @property array $daterange
 * @property int|null $adults
 * @property int|null $children
 * @property int|null $babies
 * @property bool $pets
 * @property int|null $bedrooms
 * @property string|null $status
 * @property string|null $security_deposit_status
 * @property string|null $payment_status
 * @property bool $handles_payment
 * @property int|null $expiration_hours
 * @property \Carbon\Carbon|null $expires_at
 * @property string|null $point_of_conversion
 * @property \Carbon\Carbon|null $next_payment_due_date
 * @property mixed|null $next_payment_amount
 * @property string|null $source_name
 * @property string|null $external_id
 * @property Channel|null $channel
 * @property string|null $channel_external_id
 * @property string|null $sub_channel_name
 * @property string|null $sub_channel_external_id
 * @property int|null $checkin_change_over_days
 * @property int|null $checkout_change_over_days
 * @property string|null $reason_for_travel
 * @property string|null $notes
 * @property string|null $guest_message
 * @property string|null $cancel_reason
 * @property string|null $reject_reason
 * @property string|null $reject_message_to_guest
 * @property \Carbon\Carbon|null $confirmed_at
 * @property \Carbon\Carbon|null $rejected_at
 * @property \Carbon\Carbon|null $cancelled_at
 * @property mixed|null $total
 * @property mixed|null $total_pretax
 * @property mixed|null $total_accommodation
 * @property mixed|null $total_accommodation_pretax
 * @property mixed|null $total_fees
 * @property mixed|null $total_fees_pretax
 * @property mixed|null $total_taxes
 * @property mixed|null $total_outstanding
 * @property mixed|null $total_paid
 * @property mixed|null $total_refunded
 * @property mixed|null $total_pm_share
 * @property mixed|null $accommodation_pm_share
 * @property BookingProduct[]|\Illuminate\Support\Collection $products
 * @property ServiceRequest[]|\Illuminate\Support\Collection $service_requests
 * @property string|null $payment_link
 * @property string|null $security_deposit_link
 * @property string|null $summary_url
 * @property SecurityDeposit[]|\Illuminate\Support\Collection $security_deposits
 * @property BookingGuest[]|\Illuminate\Support\Collection $additional_guests
 * @property Account|null $main_guest
 * @property BookingPaymentScheduleStep[]|\Illuminate\Support\Collection $payment_schedule_steps
 * @property BookingTaCommission|null $ta_commission
 * @property BookingHostRevenueShare|null $host_revenue_share
 * @property CreditCard[]|\Illuminate\Support\Collection $credit_cards
 * @property Stay|null $stay
 * @property Payment[]|\Illuminate\Support\Collection $payments
 * @property Booking|null $relocated_to_booking
 * @property Booking|null $relocated_from_booking
 * @property bool $has_valid_cc_attached
 * @property bool $has_automated_cc_processing
 * @property string|null $booking_category
 * @property string|null $guest_agreement_form_url
 * @property bool $protection_enabled
 * @property string|null $protection_status
 * @property string|null $protection_status_message
 * @property bool $guest_verification_enabled
 * @property string|null $guest_verification_status
 * @property string|null $guest_verification_status_message
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class Booking extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'booking';

	protected $casts = [
		'property' => '\Onemineral\PMS\SDK\Resources\Property',
		'booker' => '\Onemineral\PMS\SDK\Resources\PartnerAccount',
		'currency' => '\Onemineral\PMS\SDK\Resources\Currency',
		'checkin' => 'date',
		'checkout' => 'date',
		'pets' => 'bool',
		'handles_payment' => 'bool',
		'expires_at' => 'date',
		'next_payment_due_date' => 'date',
		'channel' => '\Onemineral\PMS\SDK\Resources\Channel',
		'confirmed_at' => 'date',
		'rejected_at' => 'date',
		'cancelled_at' => 'date',
		'products' => '\Onemineral\PMS\SDK\Resources\BookingProduct',
		'service_requests' => '\Onemineral\PMS\SDK\Resources\ServiceRequest',
		'security_deposits' => '\Onemineral\PMS\SDK\Resources\SecurityDeposit',
		'additional_guests' => '\Onemineral\PMS\SDK\Resources\BookingGuest',
		'main_guest' => '\Onemineral\PMS\SDK\Resources\Account',
		'payment_schedule_steps' => '\Onemineral\PMS\SDK\Resources\BookingPaymentScheduleStep',
		'ta_commission' => '\Onemineral\PMS\SDK\Resources\BookingTaCommission',
		'host_revenue_share' => '\Onemineral\PMS\SDK\Resources\BookingHostRevenueShare',
		'credit_cards' => '\Onemineral\PMS\SDK\Resources\CreditCard',
		'stay' => '\Onemineral\PMS\SDK\Resources\Stay',
		'payments' => '\Onemineral\PMS\SDK\Resources\Payment',
		'relocated_to_booking' => '\Onemineral\PMS\SDK\Resources\Booking',
		'relocated_from_booking' => '\Onemineral\PMS\SDK\Resources\Booking',
		'has_valid_cc_attached' => 'bool',
		'has_automated_cc_processing' => 'bool',
		'protection_enabled' => 'bool',
		'guest_verification_enabled' => 'bool',
		'created_at' => 'date',
		'updated_at' => 'date',
	];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function query(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/query", "\Onemineral\PMS\SDK\Resources\Booking");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function autocomplete(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/autocomplete", "\Onemineral\PMS\SDK\Resources\Booking");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function fetch(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/fetch", "\Onemineral\PMS\SDK\Resources\Booking");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update", "\Onemineral\PMS\SDK\Resources\Booking");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update_stay(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update-stay", "\Onemineral\PMS\SDK\Resources\Booking");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update_homeowner_stay(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update-homeowner-stay", "\Onemineral\PMS\SDK\Resources\Booking");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function import_booking(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/import-booking", "\Onemineral\PMS\SDK\Resources\Booking");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function import_bookings(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/import-bookings", "\Onemineral\PMS\SDK\Resources\ImportBulkBooking");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function import_from_file(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/import-from-file", "\Onemineral\PMS\SDK\Resources\Mutation");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update_ta_commission(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update-ta-commission", "\Onemineral\PMS\SDK\Resources\BookingTaCommission");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update_host_revenue_share(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update-host-revenue-share", "\Onemineral\PMS\SDK\Resources\BookingHostRevenueShare");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update_payment_schedule(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update-payment-schedule", "\Onemineral\PMS\SDK\Resources\BookingPaymentScheduleStep");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function fetch_payments(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/fetch-payments", "\Onemineral\PMS\SDK\Resources\Payment");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function export(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/export", "\Onemineral\PMS\SDK\Resources\Booking");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function report(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/report", "\Onemineral\PMS\SDK\Resources\Booking");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function cancel(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/cancel", "\Onemineral\PMS\SDK\Resources\Booking");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function confirm(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/confirm", "\Onemineral\PMS\SDK\Resources\Booking");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function complete(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/complete", "\Onemineral\PMS\SDK\Resources\Booking");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function expire(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/expire", "\Onemineral\PMS\SDK\Resources\Booking");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function hold(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/hold", "\Onemineral\PMS\SDK\Resources\Booking");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function pending_host_confirmation(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/pending-host-confirmation", "\Onemineral\PMS\SDK\Resources\Booking");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function pending_payment(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/pending-payment", "\Onemineral\PMS\SDK\Resources\Booking");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function reject(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/reject", "\Onemineral\PMS\SDK\Resources\Booking");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function void(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/void", "\Onemineral\PMS\SDK\Resources\Booking");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function sync_availability(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/sync-availability", "\Onemineral\PMS\SDK\Resources\Mutation");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function relocate(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/relocate", "\Onemineral\PMS\SDK\Resources\Booking");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function generate_payment_link(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/generate-payment-link", "\Onemineral\PMS\SDK\Resources\Booking");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function sync_additional_guests(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/sync-additional-guests", "\Onemineral\PMS\SDK\Resources\BookingGuest");
	}
}
