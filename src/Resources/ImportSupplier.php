<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class ImportSupplier extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'import-supplier';
	protected $casts = ['created_at' => 'date', 'updated_at' => 'date'];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function create(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/create", "\Onemineral\PMS\SDK\Resources\ImportSupplier");
	}
}
