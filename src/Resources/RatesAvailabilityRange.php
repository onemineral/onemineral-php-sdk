<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property array $daterange
 * @property mixed|null $rate
 * @property int|null $min_stay
 * @property bool $checkin_restricted
 * @property bool $checkout_restricted
 * @property string|null $availability_status_type
 * @property bool $is_available
 */
class RatesAvailabilityRange extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'rates-availability-range';
	protected $casts = ['checkin_restricted' => 'bool', 'checkout_restricted' => 'bool', 'is_available' => 'bool'];
}
