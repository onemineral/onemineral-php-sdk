<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property Channel|null $channel
 * @property mixed|null $resource
 * @property string|null $status
 * @property string|null $provider
 * @property string|null $event
 * @property int|null $changes
 * @property \Carbon\Carbon|null $last_sync_at
 * @property \Carbon\Carbon|null $last_successful_sync_at
 * @property string|null $last_sync_error_message
 * @property string|null $last_sync_status
 * @property int|null $last_sync_changes
 * @property int|null $retries
 * @property array $warnings
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class ChannelManagerSync extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'channel-manager-sync';

	protected $casts = [
		'channel' => '\Onemineral\PMS\SDK\Resources\Channel',
		'last_sync_at' => 'date',
		'last_successful_sync_at' => 'date',
		'created_at' => 'date',
		'updated_at' => 'date',
	];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function query(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/query", "\Onemineral\PMS\SDK\Resources\ChannelManagerSync");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function fetch(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/fetch", "\Onemineral\PMS\SDK\Resources\ChannelManagerSync");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function retry(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/retry", "\Onemineral\PMS\SDK\Resources\ChannelManagerSync");
	}
}
