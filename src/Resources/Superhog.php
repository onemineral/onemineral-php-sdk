<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property string|null $name
 * @property string|null $external_id
 * @property string|null $status
 * @property array $options
 * @property string|null $provider
 * @property string|null $logo_url
 * @property string|null $icon_url
 * @property Account|null $account
 * @property float $markup
 * @property ChannelProperty|null $connection
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class Superhog extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'superhog';

	protected $casts = [
		'account' => '\Onemineral\PMS\SDK\Resources\Account',
		'markup' => 'float',
		'connection' => '\Onemineral\PMS\SDK\Resources\ChannelProperty',
		'created_at' => 'date',
		'updated_at' => 'date',
	];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update_settings(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update-settings", "\Onemineral\PMS\SDK\Resources\Superhog");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function fetch_settings(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/fetch-settings", "\Onemineral\PMS\SDK\Resources\Superhog");
	}
}
