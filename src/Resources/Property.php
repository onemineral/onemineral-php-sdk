<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property Image[]|\Illuminate\Support\Collection $images
 * @property Image|null $main_image
 * @property int $id
 * @property array $name
 * @property string|null $internal_name
 * @property array $headline
 * @property array $short_description
 * @property array $description
 * @property array $house_rules
 * @property array $surroundings
 * @property array $unique_benefits
 * @property string|null $status
 * @property string|null $ical_url
 * @property string|null $calendar_url
 * @property string|null $brochure_url
 * @property string|null $rental_agreement_url
 * @property string|null $rental_agreement_pdf_url
 * @property Location|null $location
 * @property PropertyType|null $property_type
 * @property Currency|null $currency
 * @property int|null $bedrooms
 * @property int|null $bathrooms
 * @property int|null $toilets
 * @property int|null $max_occupancy
 * @property int|null $comfortable_occupancy
 * @property string|null $registration_number
 * @property \Carbon\Carbon|null $registration_number_issue_date
 * @property string|null $registration_number_jurisdiction
 * @property string|null $address
 * @property string|null $postcode
 * @property array $geo
 * @property array $display_geo
 * @property bool $show_exact_address
 * @property string|null $price_type
 * @property string|null $external_id
 * @property mixed|null $marketing_price
 * @property string|null $checkin_category
 * @property mixed|null $interior_size
 * @property mixed|null $exterior_size
 * @property array $checkin_instructions
 * @property array $home_truths
 * @property int|null $reviews_count
 * @property float $reviews_rating
 * @property bool $website_visible
 * @property int|null $score
 * @property Amenity[]|\Illuminate\Support\Collection $amenities
 * @property PropertyRatesSettings|null $rates_settings
 * @property PropertyHostRevenueShare|null $host_revenue_share
 * @property PartnerAccount[]|\Illuminate\Support\Collection $homeowners
 * @property Discount[]|\Illuminate\Support\Collection $discounts
 * @property \Carbon\Carbon|null $rates_updated_at
 * @property \Carbon\Carbon|null $availability_updated_at
 * @property PropertyPolicy|null $policies
 * @property PropertyFee[]|\Illuminate\Support\Collection $fees
 * @property PropertyLosSeason[]|\Illuminate\Support\Collection $los_seasons
 * @property Review[]|\Illuminate\Support\Collection $reviews
 * @property Booking[]|\Illuminate\Support\Collection $bookings
 * @property RatesAvailability[]|\Illuminate\Support\Collection $rates_availability
 * @property ChannelManagerSync[]|\Illuminate\Support\Collection $channel_manager_sync
 * @property ChannelProperty[]|\Illuminate\Support\Collection $channel_connections
 * @property Seo|null $seo
 * @property LegalEntity|null $legal_entity
 * @property Property[]|\Illuminate\Support\Collection $related_properties
 * @property Collection[]|\Illuminate\Support\Collection $collections
 * @property Tag[]|\Illuminate\Support\Collection $tags
 * @property RentalAgreement|null $rental_agreement
 * @property PropertyRoom[]|\Illuminate\Support\Collection $rooms
 * @property PropertyIcal[]|\Illuminate\Support\Collection $property_icals
 * @property AvailabilityDetails|null $availability_details
 * @property Image[]|\Illuminate\Support\Collection $floor_images
 * @property HealthScore[]|\Illuminate\Support\Collection $health_scores
 * @property int|null $health_score_total
 * @property int|null $health_score_done
 * @property float $health_score_done_percent
 * @property int|null $critical_score_total
 * @property int|null $critical_score_done
 * @property float $critical_score_done_percent
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class Property extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'property';

	protected $casts = [
		'images' => '\Onemineral\PMS\SDK\Resources\Image',
		'main_image' => '\Onemineral\PMS\SDK\Resources\Image',
		'location' => '\Onemineral\PMS\SDK\Resources\Location',
		'property_type' => '\Onemineral\PMS\SDK\Resources\PropertyType',
		'currency' => '\Onemineral\PMS\SDK\Resources\Currency',
		'registration_number_issue_date' => 'date',
		'show_exact_address' => 'bool',
		'reviews_rating' => 'float',
		'website_visible' => 'bool',
		'amenities' => '\Onemineral\PMS\SDK\Resources\Amenity',
		'rates_settings' => '\Onemineral\PMS\SDK\Resources\PropertyRatesSettings',
		'host_revenue_share' => '\Onemineral\PMS\SDK\Resources\PropertyHostRevenueShare',
		'homeowners' => '\Onemineral\PMS\SDK\Resources\PartnerAccount',
		'discounts' => '\Onemineral\PMS\SDK\Resources\Discount',
		'rates_updated_at' => 'date',
		'availability_updated_at' => 'date',
		'policies' => '\Onemineral\PMS\SDK\Resources\PropertyPolicy',
		'fees' => '\Onemineral\PMS\SDK\Resources\PropertyFee',
		'los_seasons' => '\Onemineral\PMS\SDK\Resources\PropertyLosSeason',
		'reviews' => '\Onemineral\PMS\SDK\Resources\Review',
		'bookings' => '\Onemineral\PMS\SDK\Resources\Booking',
		'rates_availability' => '\Onemineral\PMS\SDK\Resources\RatesAvailability',
		'channel_manager_sync' => '\Onemineral\PMS\SDK\Resources\ChannelManagerSync',
		'channel_connections' => '\Onemineral\PMS\SDK\Resources\ChannelProperty',
		'seo' => '\Onemineral\PMS\SDK\Resources\Seo',
		'legal_entity' => '\Onemineral\PMS\SDK\Resources\LegalEntity',
		'related_properties' => '\Onemineral\PMS\SDK\Resources\Property',
		'collections' => '\Onemineral\PMS\SDK\Resources\Collection',
		'tags' => '\Onemineral\PMS\SDK\Resources\Tag',
		'rental_agreement' => '\Onemineral\PMS\SDK\Resources\RentalAgreement',
		'rooms' => '\Onemineral\PMS\SDK\Resources\PropertyRoom',
		'property_icals' => '\Onemineral\PMS\SDK\Resources\PropertyIcal',
		'availability_details' => '\Onemineral\PMS\SDK\Resources\AvailabilityDetails',
		'floor_images' => '\Onemineral\PMS\SDK\Resources\Image',
		'health_scores' => '\Onemineral\PMS\SDK\Resources\HealthScore',
		'health_score_done_percent' => 'float',
		'critical_score_done_percent' => 'float',
		'created_at' => 'date',
		'updated_at' => 'date',
	];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function query(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/query", "\Onemineral\PMS\SDK\Resources\Property");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function fetch(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/fetch", "\Onemineral\PMS\SDK\Resources\Property");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function create(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/create", "\Onemineral\PMS\SDK\Resources\Property");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update", "\Onemineral\PMS\SDK\Resources\Property");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function set_status(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/set-status", "\Onemineral\PMS\SDK\Resources\Property");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function history(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/history", "\Onemineral\PMS\SDK\Resources\Activity");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function autocomplete(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/autocomplete", "\Onemineral\PMS\SDK\Resources\Property");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function search(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/search", "\Onemineral\PMS\SDK\Resources\Search");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function import(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/import", "\Onemineral\PMS\SDK\Resources\Property");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function upload_image(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/upload-image", "\Onemineral\PMS\SDK\Resources\Image");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function delete_image(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/delete-image", "\Onemineral\PMS\SDK\Resources\Mutation");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update_image(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update-image", "\Onemineral\PMS\SDK\Resources\Image");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function order_images(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/order-images", "\Onemineral\PMS\SDK\Resources\Mutation");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function set_availability(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/set-availability", "\Onemineral\PMS\SDK\Resources\Mutation");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function set_rates_availability(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/set-rates-availability", "\Onemineral\PMS\SDK\Resources\Mutation");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function delete_availability(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/delete-availability", "\Onemineral\PMS\SDK\Resources\Mutation");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function refresh_availability(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/refresh-availability", "\Onemineral\PMS\SDK\Resources\Mutation");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function sync_blocked_availability(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/sync-blocked-availability", "\Onemineral\PMS\SDK\Resources\Property");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function get_rates_availability(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/get-rates-availability", "\Onemineral\PMS\SDK\Resources\RatesAvailability");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function get_los_rates(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/get-los-rates", "\Onemineral\PMS\SDK\Resources\Mutation");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function get_unavailability(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/get-unavailability", "\Onemineral\PMS\SDK\Resources\Unavailability");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function rates_with_ranges(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/rates-with-ranges", "\Onemineral\PMS\SDK\Resources\RatesAvailabilityRange");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function copy_rates(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/copy-rates", "\Onemineral\PMS\SDK\Resources\Mutation");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function get_occupancy_rates(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/get-occupancy-rates", "\Onemineral\PMS\SDK\Resources\PropertyOccupancySeason");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function set_occupancy_rates(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/set-occupancy-rates", "\Onemineral\PMS\SDK\Resources\PropertyOccupancySeason");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update_policies(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update-policies", "\Onemineral\PMS\SDK\Resources\PropertyPolicy");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update_rates_settings(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update-rates-settings", "\Onemineral\PMS\SDK\Resources\PropertyRatesSettings");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function bulk_update(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/bulk-update", "\Onemineral\PMS\SDK\Resources\Mutation");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function bulk_update_policies(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/bulk-update-policies", "\Onemineral\PMS\SDK\Resources\Mutation");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function bulk_update_rates_settings(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/bulk-update-rates-settings", "\Onemineral\PMS\SDK\Resources\Mutation");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function bulk_set_rates_availability(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/bulk-set-rates-availability", "\Onemineral\PMS\SDK\Resources\Mutation");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function bulk_sync_fees(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/bulk-sync-fees", "\Onemineral\PMS\SDK\Resources\Mutation");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function bulk_copy_rates(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/bulk-copy-rates", "\Onemineral\PMS\SDK\Resources\Mutation");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update_host_revenue_share(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update-host-revenue-share", "\Onemineral\PMS\SDK\Resources\PropertyHostRevenueShare");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function get_applicable_discounts(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/get-applicable-discounts", "\Onemineral\PMS\SDK\Resources\ApplicableDiscount");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function sync_fees(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/sync-fees", "\Onemineral\PMS\SDK\Resources\PropertyFee");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function sync_tags(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/sync-tags", "\Onemineral\PMS\SDK\Resources\Tag");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function get_quote(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/get-quote", "\Onemineral\PMS\SDK\Resources\Quote");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function get_homeowner_quote(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/get-homeowner-quote", "\Onemineral\PMS\SDK\Resources\Quote");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function create_booking(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/create-booking", "\Onemineral\PMS\SDK\Resources\Booking");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function create_homeowner_booking(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/create-homeowner-booking", "\Onemineral\PMS\SDK\Resources\Booking");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function upload_floor_image(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/upload-floor-image", "\Onemineral\PMS\SDK\Resources\Image");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function delete_floor_image(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/delete-floor-image", "\Onemineral\PMS\SDK\Resources\Mutation");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update_floor_image(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update-floor-image", "\Onemineral\PMS\SDK\Resources\Image");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function order_floor_images(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/order-floor-images", "\Onemineral\PMS\SDK\Resources\Mutation");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function channel_errors(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/channel-errors", "\Onemineral\PMS\SDK\Resources\Property");
	}
}
