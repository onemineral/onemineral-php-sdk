<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property mixed|null $parent_resource
 * @property string|null $name
 * @property string|null $description
 * @property string|null $guest_request
 * @property int|null $items_count
 * @property string|null $type
 * @property \Carbon\Carbon|null $checkin
 * @property \Carbon\Carbon|null $checkout
 * @property mixed|null $total_pretax
 * @property mixed|null $total
 * @property bool $include_in_accommodation_subtotal
 * @property bool $read_only
 * @property mixed|null $resource
 * @property BookingProductLineItem[]|\Illuminate\Support\Collection $line_items
 * @property CalculatedTax[]|\Illuminate\Support\Collection $taxes
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class BookingProduct extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'booking-product';

	protected $casts = [
		'checkin' => 'date',
		'checkout' => 'date',
		'include_in_accommodation_subtotal' => 'bool',
		'read_only' => 'bool',
		'line_items' => '\Onemineral\PMS\SDK\Resources\BookingProductLineItem',
		'taxes' => '\Onemineral\PMS\SDK\Resources\CalculatedTax',
		'created_at' => 'date',
		'updated_at' => 'date',
	];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function fetch(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/fetch", "\Onemineral\PMS\SDK\Resources\BookingProduct");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function create(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/create", "\Onemineral\PMS\SDK\Resources\BookingProduct");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update", "\Onemineral\PMS\SDK\Resources\BookingProduct");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function delete(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/delete", "\Onemineral\PMS\SDK\Resources\Mutation");
	}
}
