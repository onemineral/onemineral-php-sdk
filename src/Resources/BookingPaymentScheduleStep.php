<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property string|null $short_description
 * @property string|null $type
 * @property int|null $days
 * @property float $charge_percent
 * @property \Carbon\Carbon|null $due_date
 * @property mixed|null $amount
 * @property mixed|null $amount_paid
 * @property bool $is_paid
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class BookingPaymentScheduleStep extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'booking-payment-schedule-step';

	protected $casts = [
		'charge_percent' => 'float',
		'due_date' => 'date',
		'is_paid' => 'bool',
		'created_at' => 'date',
		'updated_at' => 'date',
	];
}
