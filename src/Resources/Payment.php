<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property mixed|null $resource
 * @property mixed|null $amount
 * @property mixed|null $amount_converted
 * @property string|null $notes
 * @property string|null $description
 * @property string|null $type
 * @property string|null $status
 * @property string|null $error
 * @property string|null $external_id
 * @property CreditCard|null $credit_card
 * @property PaymentMethod|null $payment_method
 * @property SecurityDeposit|null $security_deposit
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class Payment extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'payment';

	protected $casts = [
		'credit_card' => '\Onemineral\PMS\SDK\Resources\CreditCard',
		'payment_method' => '\Onemineral\PMS\SDK\Resources\PaymentMethod',
		'security_deposit' => '\Onemineral\PMS\SDK\Resources\SecurityDeposit',
		'created_at' => 'date',
		'updated_at' => 'date',
	];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function create(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/create", "\Onemineral\PMS\SDK\Resources\Payment");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function create_refund(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/create-refund", "\Onemineral\PMS\SDK\Resources\Payment");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function refund(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/refund", "\Onemineral\PMS\SDK\Resources\Payment");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function void(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/void", "\Onemineral\PMS\SDK\Resources\Payment");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function fetch(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/fetch", "\Onemineral\PMS\SDK\Resources\Payment");
	}
}
