<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property array $name
 * @property string|null $category
 * @property string|null $type
 * @property array $subject
 * @property array $body
 * @property array $content
 * @property bool $enabled
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class Notification extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'notification';
	protected $casts = ['enabled' => 'bool', 'created_at' => 'date', 'updated_at' => 'date'];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function query(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/query", "\Onemineral\PMS\SDK\Resources\Notification");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function fetch(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/fetch", "\Onemineral\PMS\SDK\Resources\Notification");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update", "\Onemineral\PMS\SDK\Resources\Notification");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function enable(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/enable", "\Onemineral\PMS\SDK\Resources\Notification");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function disable(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/disable", "\Onemineral\PMS\SDK\Resources\Notification");
	}
}
