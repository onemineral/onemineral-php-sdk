<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int|null $id
 * @property bool $success
 * @property array $data
 */
class Mutation extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'mutation';
	protected $casts = ['success' => 'bool'];
}
