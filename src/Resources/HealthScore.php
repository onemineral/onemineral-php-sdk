<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property mixed|null $resource
 * @property string|null $provider
 * @property string|null $check_id
 * @property string|null $description
 * @property string|null $level
 * @property bool $done
 * @property int|null $score
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class HealthScore extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'health-score';
	protected $casts = ['done' => 'bool', 'created_at' => 'date', 'updated_at' => 'date'];
}
