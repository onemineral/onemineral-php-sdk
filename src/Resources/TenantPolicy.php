<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property int|null $min_stay
 * @property int|null $max_stay
 * @property int|null $min_prior_notify
 * @property int|null $min_children_age
 * @property int|null $min_adults_age
 * @property int|null $adults_age_from
 * @property PaymentSchedule|null $default_payment_schedule
 */
class TenantPolicy extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'tenant-policy';
	protected $casts = ['default_payment_schedule' => '\Onemineral\PMS\SDK\Resources\PaymentSchedule'];
}
