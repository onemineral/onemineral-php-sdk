<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property string|null $booking_external_id
 * @property bool $success
 * @property string|null $error_message
 * @property array $errors
 */
class ImportBulkBooking extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'import-bulk-booking';
	protected $casts = ['success' => 'bool'];
}
