<?php 

namespace Onemineral\PMS\SDK\Resources;

class Report extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'report';
	protected $casts = [];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function occupancy_per_property(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/occupancy-per-property", "\Onemineral\PMS\SDK\Resources\Report");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function overlapping_bookings(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/overlapping-bookings", "\Onemineral\PMS\SDK\Resources\Report");
	}
}
