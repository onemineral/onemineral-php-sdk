<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property Image[]|\Illuminate\Support\Collection $images
 * @property Image|null $main_image
 * @property int $id
 * @property array $name
 * @property Country|null $country
 * @property array $description
 * @property array $geo
 * @property Location|null $parent_location
 * @property Location[]|\Illuminate\Support\Collection $ancestors
 * @property Collection[]|\Illuminate\Support\Collection $collections
 * @property Tag[]|\Illuminate\Support\Collection $tags
 * @property Property[]|\Illuminate\Support\Collection $properties
 * @property Seo|null $seo
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class Location extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'location';

	protected $casts = [
		'images' => '\Onemineral\PMS\SDK\Resources\Image',
		'main_image' => '\Onemineral\PMS\SDK\Resources\Image',
		'country' => '\Onemineral\PMS\SDK\Resources\Country',
		'parent_location' => '\Onemineral\PMS\SDK\Resources\Location',
		'ancestors' => '\Onemineral\PMS\SDK\Resources\Location',
		'collections' => '\Onemineral\PMS\SDK\Resources\Collection',
		'tags' => '\Onemineral\PMS\SDK\Resources\Tag',
		'properties' => '\Onemineral\PMS\SDK\Resources\Property',
		'seo' => '\Onemineral\PMS\SDK\Resources\Seo',
		'created_at' => 'date',
		'updated_at' => 'date',
	];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function query(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/query", "\Onemineral\PMS\SDK\Resources\Location");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function fetch(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/fetch", "\Onemineral\PMS\SDK\Resources\Location");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function create(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/create", "\Onemineral\PMS\SDK\Resources\Location");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update", "\Onemineral\PMS\SDK\Resources\Location");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function delete(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/delete", "\Onemineral\PMS\SDK\Resources\Mutation");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function history(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/history", "\Onemineral\PMS\SDK\Resources\Activity");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function autocomplete(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/autocomplete", "\Onemineral\PMS\SDK\Resources\Location");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function upload_image(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/upload-image", "\Onemineral\PMS\SDK\Resources\Image");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update_image(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update-image", "\Onemineral\PMS\SDK\Resources\Image");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function delete_image(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/delete-image", "\Onemineral\PMS\SDK\Resources\Mutation");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function order_images(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/order-images", "\Onemineral\PMS\SDK\Resources\Mutation");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function sync_tags(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/sync-tags", "\Onemineral\PMS\SDK\Resources\Tag");
	}
}
