<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property string|null $name
 * @property string|null $subdomain
 * @property Currency|null $default_currency
 * @property Language|null $default_language
 * @property Image|null $logo_light
 * @property Image|null $logo_dark
 * @property TenantSettings|null $settings
 * @property TenantPolicy|null $policies
 * @property string|null $public_multicalendar_url
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class Tenant extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'tenant';

	protected $casts = [
		'default_currency' => '\Onemineral\PMS\SDK\Resources\Currency',
		'default_language' => '\Onemineral\PMS\SDK\Resources\Language',
		'logo_light' => '\Onemineral\PMS\SDK\Resources\Image',
		'logo_dark' => '\Onemineral\PMS\SDK\Resources\Image',
		'settings' => '\Onemineral\PMS\SDK\Resources\TenantSettings',
		'policies' => '\Onemineral\PMS\SDK\Resources\TenantPolicy',
		'created_at' => 'date',
		'updated_at' => 'date',
	];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update_settings(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update-settings", "\Onemineral\PMS\SDK\Resources\Tenant");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update_policies(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update-policies", "\Onemineral\PMS\SDK\Resources\Tenant");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update", "\Onemineral\PMS\SDK\Resources\Tenant");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function upload_logo_dark(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/upload-logo-dark", "\Onemineral\PMS\SDK\Resources\Image");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function upload_logo_light(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/upload-logo-light", "\Onemineral\PMS\SDK\Resources\Image");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function fetch(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/fetch", "\Onemineral\PMS\SDK\Resources\Tenant");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function query(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/query", "\Onemineral\PMS\SDK\Resources\Tenant");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function current(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/current", "\Onemineral\PMS\SDK\Resources\Tenant");
	}
}
