<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int|null $base_occupancy
 * @property AvailabilityStatus|null $default_availability_status
 * @property TaxClass|null $tax_class
 * @property PaymentSchedule|null $payment_schedule
 * @property int|null $min_stay
 * @property int|null $max_stay
 * @property string|null $changeover_days
 * @property bool $security_deposit_auto_authorize
 * @property int|null $security_deposit_authorization_days
 * @property int|null $security_deposit_release_days
 * @property mixed|null $min_nightly_rate
 * @property mixed|null $extra_guest_rate
 * @property mixed|null $security_deposit
 * @property float $ta_max_commission_percent
 */
class PropertyRatesSettings extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'property-rates-settings';

	protected $casts = [
		'default_availability_status' => '\Onemineral\PMS\SDK\Resources\AvailabilityStatus',
		'tax_class' => '\Onemineral\PMS\SDK\Resources\TaxClass',
		'payment_schedule' => '\Onemineral\PMS\SDK\Resources\PaymentSchedule',
		'security_deposit_auto_authorize' => 'bool',
		'ta_max_commission_percent' => 'float',
	];
}
