<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property array $daterange
 * @property array $discounts
 */
class ApplicableDiscount extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'applicable-discount';
	protected $casts = [];
}
