<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property Booking|null $booking
 * @property CreditCard|null $credit_card
 * @property mixed|null $amount
 * @property string|null $reason
 * @property string|null $error
 * @property string|null $external_id
 * @property Currency|null $currency
 * @property PaymentMethod|null $payment_method
 * @property User|null $created_by
 * @property string|null $status
 * @property string|null $type
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class SecurityDeposit extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'security-deposit';

	protected $casts = [
		'booking' => '\Onemineral\PMS\SDK\Resources\Booking',
		'credit_card' => '\Onemineral\PMS\SDK\Resources\CreditCard',
		'currency' => '\Onemineral\PMS\SDK\Resources\Currency',
		'payment_method' => '\Onemineral\PMS\SDK\Resources\PaymentMethod',
		'created_by' => '\Onemineral\PMS\SDK\Resources\User',
		'created_at' => 'date',
		'updated_at' => 'date',
	];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function authorize(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/authorize", "\Onemineral\PMS\SDK\Resources\SecurityDeposit");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function capture(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/capture", "\Onemineral\PMS\SDK\Resources\Payment");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function release(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/release", "\Onemineral\PMS\SDK\Resources\SecurityDeposit");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function fetch(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/fetch", "\Onemineral\PMS\SDK\Resources\SecurityDeposit");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update", "\Onemineral\PMS\SDK\Resources\SecurityDeposit");
	}
}
