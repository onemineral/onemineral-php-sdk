<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property Workflow|null $workflow
 * @property mixed|null $resource
 * @property string|null $resource_type
 * @property bool $enabled
 * @property \Carbon\Carbon|null $execute_at
 * @property string|null $timezone
 * @property bool $should_run
 * @property string|null $run_status
 * @property string|null $run_id
 * @property string|null $error
 * @property bool $conditions_passed
 * @property bool $force_run
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class WorkflowSchedule extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'workflow-schedule';

	protected $casts = [
		'workflow' => '\Onemineral\PMS\SDK\Resources\Workflow',
		'enabled' => 'bool',
		'execute_at' => 'date',
		'should_run' => 'bool',
		'conditions_passed' => 'bool',
		'force_run' => 'bool',
		'created_at' => 'date',
		'updated_at' => 'date',
	];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function query(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/query", "\Onemineral\PMS\SDK\Resources\WorkflowSchedule");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function fetch(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/fetch", "\Onemineral\PMS\SDK\Resources\WorkflowSchedule");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function enable(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/enable", "\Onemineral\PMS\SDK\Resources\WorkflowSchedule");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function disable(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/disable", "\Onemineral\PMS\SDK\Resources\WorkflowSchedule");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function force_run(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/force-run", "\Onemineral\PMS\SDK\Resources\WorkflowSchedule");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function retry(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/retry", "\Onemineral\PMS\SDK\Resources\WorkflowSchedule");
	}
}
