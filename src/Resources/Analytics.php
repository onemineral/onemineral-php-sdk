<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property array $data
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class Analytics extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'analytics';
	protected $casts = ['created_at' => 'date', 'updated_at' => 'date'];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function occupancy(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/occupancy", "\Onemineral\PMS\SDK\Resources\Analytics");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function daily_occupancy(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/daily-occupancy", "\Onemineral\PMS\SDK\Resources\Analytics");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function monthly_revenue(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/monthly-revenue", "\Onemineral\PMS\SDK\Resources\Analytics");
	}
}
