<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property Booking|null $booking
 * @property mixed|null $arrival_time
 * @property \Carbon\Carbon|null $arrival_date
 * @property bool $car_park
 * @property string|null $arrive_by
 * @property string|null $arrival_flight_number
 * @property mixed|null $arrival_flight_eta
 * @property string|null $arrival_airport
 * @property string|null $arrival_flight_status
 * @property string|null $arrival_train_number
 * @property mixed|null $arrival_train_eta
 * @property string|null $arrival_train_station
 * @property string|null $arrival_notes
 * @property mixed|null $departure_time
 * @property \Carbon\Carbon|null $departure_date
 * @property string|null $departure_by
 * @property string|null $departure_flight_number
 * @property mixed|null $departure_flight_departure_time
 * @property string|null $departure_flight_status
 * @property string|null $departure_airport
 * @property string|null $departure_train_number
 * @property mixed|null $departure_train_departure_time
 * @property string|null $departure_train_station
 * @property string|null $departure_notes
 * @property string|null $staff_notes
 * @property string|null $additional_visitors
 * @property string|null $sleeping_arrangements
 * @property Image|null $identification_document
 * @property Image|null $signature
 * @property bool $guest_agreement_signed
 * @property bool $identification_document_submitted
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class Stay extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'stay';

	protected $casts = [
		'booking' => '\Onemineral\PMS\SDK\Resources\Booking',
		'arrival_date' => 'date',
		'car_park' => 'bool',
		'departure_date' => 'date',
		'identification_document' => '\Onemineral\PMS\SDK\Resources\Image',
		'signature' => '\Onemineral\PMS\SDK\Resources\Image',
		'guest_agreement_signed' => 'bool',
		'identification_document_submitted' => 'bool',
		'created_at' => 'date',
		'updated_at' => 'date',
	];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function fetch(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/fetch", "\Onemineral\PMS\SDK\Resources\Stay");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update", "\Onemineral\PMS\SDK\Resources\Stay");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function upload_identification_document(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/upload-identification-document", "\Onemineral\PMS\SDK\Resources\Image");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function upload_signature(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/upload-signature", "\Onemineral\PMS\SDK\Resources\Image");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update_visitors(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update-visitors", "\Onemineral\PMS\SDK\Resources\Stay");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update_sleeping_arrangements(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update-sleeping-arrangements", "\Onemineral\PMS\SDK\Resources\Stay");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update_staff_notes(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update-staff-notes", "\Onemineral\PMS\SDK\Resources\Stay");
	}
}
