<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property Property|null $property
 * @property array $name
 * @property array $description
 * @property bool $en_suite_bath
 * @property string|null $type
 * @property PropertyRoomBed[]|\Illuminate\Support\Collection $beds
 * @property PropertyRoomBed[]|\Illuminate\Support\Collection $additional_bed_configurations
 * @property Image[]|\Illuminate\Support\Collection $images
 */
class PropertyRoom extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'property-room';

	protected $casts = [
		'property' => '\Onemineral\PMS\SDK\Resources\Property',
		'en_suite_bath' => 'bool',
		'beds' => '\Onemineral\PMS\SDK\Resources\PropertyRoomBed',
		'additional_bed_configurations' => '\Onemineral\PMS\SDK\Resources\PropertyRoomBed',
		'images' => '\Onemineral\PMS\SDK\Resources\Image',
	];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function fetch(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/fetch", "\Onemineral\PMS\SDK\Resources\PropertyRoom");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function create(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/create", "\Onemineral\PMS\SDK\Resources\PropertyRoom");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update", "\Onemineral\PMS\SDK\Resources\PropertyRoom");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function delete(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/delete", "\Onemineral\PMS\SDK\Resources\Mutation");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function sync(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/sync", "\Onemineral\PMS\SDK\Resources\PropertyRoom");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function sync_images(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/sync-images", "\Onemineral\PMS\SDK\Resources\Image");
	}
}
