<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property Language|null $language
 * @property string|null $number_decimals
 * @property string|null $number_decimal_separator
 * @property string|null $number_thousands_separator
 * @property string|null $date_format
 * @property string|null $time_format
 * @property string|null $timezone
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class ProfileSettings extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'profile-settings';

	protected $casts = [
		'language' => '\Onemineral\PMS\SDK\Resources\Language',
		'created_at' => 'date',
		'updated_at' => 'date',
	];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function fetch(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/fetch", "\Onemineral\PMS\SDK\Resources\ProfileSettings");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update", "\Onemineral\PMS\SDK\Resources\ProfileSettings");
	}
}
