<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property array $min_stay
 * @property array $max_stay
 * @property array $checkin_restricted
 * @property array $checkout_restricted
 * @property array $max_booking_window
 * @property array $min_prior_notify
 */
class AvailabilityDetails extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'availability-details';
	protected $casts = [];
}
