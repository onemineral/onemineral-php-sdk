<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property mixed|null $resource
 * @property Tax|null $tax
 * @property string|null $name
 * @property string|null $description
 * @property mixed|null $total
 * @property mixed|null $amount
 * @property string|null $amount_type
 * @property float $percent
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class CalculatedTax extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'calculated-tax';

	protected $casts = [
		'tax' => '\Onemineral\PMS\SDK\Resources\Tax',
		'percent' => 'float',
		'created_at' => 'date',
		'updated_at' => 'date',
	];
}
