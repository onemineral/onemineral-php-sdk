<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property string|null $status
 * @property string|null $provider
 * @property array $options
 * @property string|null $external_id
 * @property Channel|null $channel
 * @property Property|null $property
 * @property ChannelManagerSync[]|\Illuminate\Support\Collection $channel_manager_sync
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class ChannelProperty extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'channel-property';

	protected $casts = [
		'channel' => '\Onemineral\PMS\SDK\Resources\Channel',
		'property' => '\Onemineral\PMS\SDK\Resources\Property',
		'channel_manager_sync' => '\Onemineral\PMS\SDK\Resources\ChannelManagerSync',
		'created_at' => 'date',
		'updated_at' => 'date',
	];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function fetch(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/fetch", "\Onemineral\PMS\SDK\Resources\ChannelProperty");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function sync(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/sync", "\Onemineral\PMS\SDK\Resources\Mutation");
	}
}
