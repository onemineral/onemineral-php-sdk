<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property TaxClass|null $tax_class
 * @property string|null $tax_type
 * @property mixed|null $amount
 * @property string|null $amount_type
 * @property float $percent
 * @property bool $is_excluded
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class Tax extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'tax';

	protected $casts = [
		'tax_class' => '\Onemineral\PMS\SDK\Resources\TaxClass',
		'percent' => 'float',
		'is_excluded' => 'bool',
		'created_at' => 'date',
		'updated_at' => 'date',
	];
}
