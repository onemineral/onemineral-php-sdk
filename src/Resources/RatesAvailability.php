<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property \Carbon\Carbon|null $day
 * @property mixed|null $rate
 * @property AvailabilityStatus|null $availability_status
 * @property int|null $min_stay
 * @property bool $checkin_restricted
 * @property bool $checkout_restricted
 * @property string|null $availability_status_type
 * @property bool $is_available
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class RatesAvailability extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'rates-availability';

	protected $casts = [
		'day' => 'date',
		'availability_status' => '\Onemineral\PMS\SDK\Resources\AvailabilityStatus',
		'checkin_restricted' => 'bool',
		'checkout_restricted' => 'bool',
		'is_available' => 'bool',
		'created_at' => 'date',
		'updated_at' => 'date',
	];
}
