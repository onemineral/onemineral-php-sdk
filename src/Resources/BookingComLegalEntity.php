<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property string|null $provider
 * @property Channel|null $channel
 * @property string|null $legal_entity_id
 * @property string|null $legal_contact_email
 * @property string|null $legal_contact_name
 * @property string|null $legal_contact_phone
 * @property string|null $legal_name
 * @property string|null $company_name
 * @property Country|null $country
 * @property string|null $city
 * @property string|null $postcode
 * @property string|null $street
 * @property array $options
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class BookingComLegalEntity extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'booking-com-legal-entity';

	protected $casts = [
		'channel' => '\Onemineral\PMS\SDK\Resources\Channel',
		'country' => '\Onemineral\PMS\SDK\Resources\Country',
		'created_at' => 'date',
		'updated_at' => 'date',
	];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function create(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/create", "\Onemineral\PMS\SDK\Resources\BookingComLegalEntity");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function fetch(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/fetch", "\Onemineral\PMS\SDK\Resources\BookingComLegalEntity");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function query(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/query", "\Onemineral\PMS\SDK\Resources\BookingComLegalEntity");
	}
}
