<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property string|null $auth_password
 * @property string|null $webhook_password
 */
class ApiPartnerAuth extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'api-partner-auth';
	protected $casts = [];
}
