<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property Property|null $property
 * @property FeeType|null $fee_type
 * @property mixed|null $amount
 * @property float $percent
 * @property TaxClass|null $tax_class
 */
class PropertyFee extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'property-fee';

	protected $casts = [
		'property' => '\Onemineral\PMS\SDK\Resources\Property',
		'fee_type' => '\Onemineral\PMS\SDK\Resources\FeeType',
		'percent' => 'float',
		'tax_class' => '\Onemineral\PMS\SDK\Resources\TaxClass',
	];
}
