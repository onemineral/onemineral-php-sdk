<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property array $data
 * @property array $facets
 */
class Search extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'search';
	protected $casts = [];
}
