<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property string|null $booking_category
 * @property bool $babies_allowed
 * @property bool $children_allowed
 * @property bool $pets_allowed
 * @property bool $smoking_allowed
 * @property bool $parties_allowed
 * @property array $checkin_window
 * @property string|null $checkout_time
 * @property int|null $max_booking_window
 * @property string|null $min_prior_notify
 * @property int|null $min_children_age
 * @property int|null $min_adults_age
 * @property int|null $adults_age_from
 * @property array $policy_description
 */
class PropertyPolicy extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'property-policy';

	protected $casts = [
		'babies_allowed' => 'bool',
		'children_allowed' => 'bool',
		'pets_allowed' => 'bool',
		'smoking_allowed' => 'bool',
		'parties_allowed' => 'bool',
	];
}
