<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property string|null $name
 * @property string|null $provider
 * @property string[]|null $accepted_credit_cards
 * @property PaymentMethod[]|\Illuminate\Support\Collection $payment_methods
 * @property string|null $logo_url
 * @property string|null $icon_url
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class PaymentGateway extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'payment-gateway';

	protected $casts = [
		'payment_methods' => '\Onemineral\PMS\SDK\Resources\PaymentMethod',
		'created_at' => 'date',
		'updated_at' => 'date',
	];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function query(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/query", "\Onemineral\PMS\SDK\Resources\PaymentGateway");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function fetch(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/fetch", "\Onemineral\PMS\SDK\Resources\PaymentGateway");
	}
}
