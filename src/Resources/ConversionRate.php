<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property array $data
 */
class ConversionRate extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'currency-conversion-rate';
	protected $casts = [];
}
