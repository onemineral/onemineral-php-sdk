<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property string|null $utm_source
 * @property string|null $utm_content
 * @property string|null $utm_medium
 * @property string|null $utm_campaign
 * @property string|null $gclid
 * @property string|null $mclid
 * @property string|null $device
 * @property int|null $cost_us_cents
 * @property \Carbon\Carbon|null $timestamp
 */
class Tracking extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'tracking';
	protected $casts = ['timestamp' => 'date'];
}
