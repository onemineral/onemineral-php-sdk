<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property string|null $description
 * @property mixed|null $causer
 * @property mixed|null $subject
 * @property array $changes
 * @property string|null $time_elapsed
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class Activity extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'activity';
	protected $casts = ['created_at' => 'date', 'updated_at' => 'date'];
}
