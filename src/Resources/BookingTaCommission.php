<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property TaxClass|null $tax_class
 * @property float $percent
 * @property string|null $apply_on
 * @property bool $apply_with_tax
 * @property mixed|null $total
 * @property mixed|null $total_pretax
 * @property bool $is_charged_by_ta
 * @property CalculatedTax[]|\Illuminate\Support\Collection $taxes
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class BookingTaCommission extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'booking-ta-commission';

	protected $casts = [
		'tax_class' => '\Onemineral\PMS\SDK\Resources\TaxClass',
		'percent' => 'float',
		'apply_with_tax' => 'bool',
		'is_charged_by_ta' => 'bool',
		'taxes' => '\Onemineral\PMS\SDK\Resources\CalculatedTax',
		'created_at' => 'date',
		'updated_at' => 'date',
	];
}
