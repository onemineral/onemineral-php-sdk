<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property \Carbon\Carbon|null $start_date
 * @property \Carbon\Carbon|null $end_date
 */
class Unavailability extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'unavailability';
	protected $casts = ['start_date' => 'date', 'end_date' => 'date'];
}
