<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property float $credit_card_fee_percent
 * @property bool $treat_inquire_only_as_available
 * @property string|null $timezone
 * @property bool $security_deposit_auto_authorize
 * @property int|null $security_deposit_authorization_days
 * @property int|null $security_deposit_release_days
 * @property AvailabilityStatus|null $ical_availability_status
 * @property AvailabilityStatus|null $default_availability_status
 * @property RentalAgreement|null $default_rental_agreement
 * @property LegalEntity|null $default_legal_entity
 * @property string[]|null $languages_spoken
 * @property string[]|null $payment_methods
 * @property array $payment_page_details
 * @property array $security_deposit_page_details
 */
class TenantSettings extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'tenant-settings';

	protected $casts = [
		'credit_card_fee_percent' => 'float',
		'treat_inquire_only_as_available' => 'bool',
		'security_deposit_auto_authorize' => 'bool',
		'ical_availability_status' => '\Onemineral\PMS\SDK\Resources\AvailabilityStatus',
		'default_availability_status' => '\Onemineral\PMS\SDK\Resources\AvailabilityStatus',
		'default_rental_agreement' => '\Onemineral\PMS\SDK\Resources\RentalAgreement',
		'default_legal_entity' => '\Onemineral\PMS\SDK\Resources\LegalEntity',
	];
}
