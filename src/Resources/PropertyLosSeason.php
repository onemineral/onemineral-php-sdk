<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property Property|null $property
 * @property array $daterange
 * @property \Carbon\Carbon|null $start_date
 * @property \Carbon\Carbon|null $end_date
 * @property bool $is_incremental
 * @property PropertyLosSeasonDiscount[]|\Illuminate\Support\Collection $property_los_season_discounts
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class PropertyLosSeason extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'property-los-season';

	protected $casts = [
		'property' => '\Onemineral\PMS\SDK\Resources\Property',
		'start_date' => 'date',
		'end_date' => 'date',
		'is_incremental' => 'bool',
		'property_los_season_discounts' => '\Onemineral\PMS\SDK\Resources\PropertyLosSeasonDiscount',
		'created_at' => 'date',
		'updated_at' => 'date',
	];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function create(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/create", "\Onemineral\PMS\SDK\Resources\PropertyLosSeason");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update", "\Onemineral\PMS\SDK\Resources\PropertyLosSeason");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function fetch(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/fetch", "\Onemineral\PMS\SDK\Resources\PropertyLosSeason");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function delete(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/delete", "\Onemineral\PMS\SDK\Resources\Mutation");
	}
}
