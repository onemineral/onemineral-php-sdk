<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property string|null $scope
 * @property string|null $role
 * @property string|null $provider
 * @property string|null $external_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class Identity extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'identity';
	protected $casts = ['created_at' => 'date', 'updated_at' => 'date'];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function delete(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/delete", "\Onemineral\PMS\SDK\Resources\Mutation");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function fetch(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/fetch", "\Onemineral\PMS\SDK\Resources\Identity");
	}
}
