<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property TaxClass|null $tax_class
 * @property string|null $type
 * @property mixed|null $service_fee_amount
 * @property float $host_percent
 * @property string|null $apply_on
 * @property bool $apply_with_tax
 * @property bool $exclude_ta_commission
 * @property mixed|null $total
 * @property mixed|null $total_pretax
 * @property CalculatedTax[]|\Illuminate\Support\Collection $taxes
 */
class BookingHostRevenueShare extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'booking-host-revenue-share';

	protected $casts = [
		'tax_class' => '\Onemineral\PMS\SDK\Resources\TaxClass',
		'host_percent' => 'float',
		'apply_with_tax' => 'bool',
		'exclude_ta_commission' => 'bool',
		'taxes' => '\Onemineral\PMS\SDK\Resources\CalculatedTax',
	];
}
