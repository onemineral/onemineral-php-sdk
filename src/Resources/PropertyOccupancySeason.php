<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property Property|null $property
 * @property LosTemplate|null $los_template
 * @property string|null $name
 * @property array $daterange
 * @property string|null $notes
 * @property int|null $min_stay
 * @property bool $is_incremental
 * @property PropertyOccupancyRate[]|\Illuminate\Support\Collection $occupancy_rates
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class PropertyOccupancySeason extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'property-occupancy-season';

	protected $casts = [
		'property' => '\Onemineral\PMS\SDK\Resources\Property',
		'los_template' => '\Onemineral\PMS\SDK\Resources\LosTemplate',
		'is_incremental' => 'bool',
		'occupancy_rates' => '\Onemineral\PMS\SDK\Resources\PropertyOccupancyRate',
		'created_at' => 'date',
		'updated_at' => 'date',
	];
}
