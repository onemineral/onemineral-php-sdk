<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property string|null $name
 * @property string|null $external_id
 * @property string|null $status
 * @property array $options
 * @property string|null $provider
 * @property string|null $logo_url
 * @property string|null $icon_url
 * @property Account|null $account
 * @property float $markup
 * @property ChannelProperty|null $connection
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class Airbnb extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'airbnb';

	protected $casts = [
		'account' => '\Onemineral\PMS\SDK\Resources\Account',
		'markup' => 'float',
		'connection' => '\Onemineral\PMS\SDK\Resources\ChannelProperty',
		'created_at' => 'date',
		'updated_at' => 'date',
	];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function redirect(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/redirect", "\Onemineral\PMS\SDK\Resources\Airbnb");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function callback(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/callback", "\Onemineral\PMS\SDK\Resources\Mutation");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function get_all_listings(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/get-all-listings", "\Onemineral\PMS\SDK\Resources\Property");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function fetch(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/fetch", "\Onemineral\PMS\SDK\Resources\Airbnb");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function import(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/import", "\Onemineral\PMS\SDK\Resources\Mutation");
	}
}
