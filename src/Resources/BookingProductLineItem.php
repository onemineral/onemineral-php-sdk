<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property BookingProduct|null $booking_product
 * @property string|null $name
 * @property string|null $description
 * @property string|null $type
 * @property mixed|null $total_pretax
 * @property mixed|null $unit_pretax
 * @property int|null $quantity
 * @property \Carbon\Carbon|null $start_date
 * @property \Carbon\Carbon|null $end_date
 * @property mixed|null $amount
 * @property string|null $amount_type
 * @property float $percent
 * @property string|null $calculation_mode
 * @property mixed|null $resource
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class BookingProductLineItem extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'booking-product-line-item';

	protected $casts = [
		'booking_product' => '\Onemineral\PMS\SDK\Resources\BookingProduct',
		'start_date' => 'date',
		'end_date' => 'date',
		'percent' => 'float',
		'created_at' => 'date',
		'updated_at' => 'date',
	];
}
