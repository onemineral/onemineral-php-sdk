<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property string|null $from_name
 * @property string|null $from_email
 * @property bool $is_default
 * @property bool $is_confirmed
 * @property string|null $domain
 * @property string|null $dkim_host
 * @property string|null $dkim_value
 * @property bool $dkim_verified
 * @property string|null $spf_host
 * @property string|null $spf_value
 * @property bool $spf_verified
 * @property string|null $return_path_host
 * @property string|null $return_path_value
 * @property bool $return_path_verified
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class EmailConfiguration extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'email-configuration';

	protected $casts = [
		'is_default' => 'bool',
		'is_confirmed' => 'bool',
		'dkim_verified' => 'bool',
		'spf_verified' => 'bool',
		'return_path_verified' => 'bool',
		'created_at' => 'date',
		'updated_at' => 'date',
	];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function query(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/query", "\Onemineral\PMS\SDK\Resources\EmailConfiguration");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function fetch(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/fetch", "\Onemineral\PMS\SDK\Resources\EmailConfiguration");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function create(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/create", "\Onemineral\PMS\SDK\Resources\EmailConfiguration");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update", "\Onemineral\PMS\SDK\Resources\EmailConfiguration");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function delete(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/delete", "\Onemineral\PMS\SDK\Resources\Mutation");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function verify(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/verify", "\Onemineral\PMS\SDK\Resources\EmailConfiguration");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function resend_confirmation(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/resend-confirmation", "\Onemineral\PMS\SDK\Resources\EmailConfiguration");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function update_default(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/update-default", "\Onemineral\PMS\SDK\Resources\EmailConfiguration");
	}
}
