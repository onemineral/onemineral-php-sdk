<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property array $description
 * @property string[]|null $tags
 * @property int|null $order
 * @property int|null $width
 * @property int|null $height
 * @property string|null $file_name
 * @property string|null $thumbnail
 * @property string|null $medium
 * @property string|null $large
 * @property string|null $original
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class Image extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'image';
	protected $casts = ['created_at' => 'date', 'updated_at' => 'date'];
}
