<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int|null $bed_count
 * @property int|null $group
 * @property BedType|null $bed_type
 */
class PropertyRoomBed extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'property-room-bed';
	protected $casts = ['bed_type' => '\Onemineral\PMS\SDK\Resources\BedType'];
}
