<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property string|null $external_id
 * @property string|null $notes
 * @property PropertyIcal|null $property_ical
 * @property array $daterange
 * @property AvailabilityStatus|null $availability_status
 * @property bool $is_available
 */
class Availability extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'availability';

	protected $casts = [
		'property_ical' => '\Onemineral\PMS\SDK\Resources\PropertyIcal',
		'availability_status' => '\Onemineral\PMS\SDK\Resources\AvailabilityStatus',
		'is_available' => 'bool',
	];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function delete(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/delete", "\Onemineral\PMS\SDK\Resources\Mutation");
	}
}
