<?php 

namespace Onemineral\PMS\SDK\Resources;

/**
 * @property int $id
 * @property string|null $description
 * @property string|null $card_holder_name
 * @property string|null $card_number_masked
 * @property string|null $card_type
 * @property string|null $card_expiration_month
 * @property string|null $card_expiration_year
 * @property string|null $status
 * @property bool $allow_automated_processing
 * @property PaymentMethod|null $payment_method
 * @property mixed|null $resource
 */
class CreditCard extends \Onemineral\PMS\SDK\ApiResource
{
	protected string $path = 'credit-card';

	protected $casts = [
		'allow_automated_processing' => 'bool',
		'payment_method' => '\Onemineral\PMS\SDK\Resources\PaymentMethod',
	];


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function create(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/create", "\Onemineral\PMS\SDK\Resources\CreditCard");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function expire(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/expire", "\Onemineral\PMS\SDK\Resources\CreditCard");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function delete(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/delete", "\Onemineral\PMS\SDK\Resources\Mutation");
	}


	/**
	 * @param array $params
	 * @return \Onemineral\PMS\SDK\Request
	 * @throws \Exception
	 */
	public function fetch(array $params = []): \Onemineral\PMS\SDK\Request
	{
		return new \Onemineral\PMS\SDK\Request($params, $this->path . "/fetch", "\Onemineral\PMS\SDK\Resources\CreditCard");
	}
}
