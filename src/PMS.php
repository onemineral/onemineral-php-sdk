<?php


namespace Onemineral\PMS\SDK;

use Illuminate\Http\Client\Factory;

class PMS extends PMSProxy
{

    protected static string $baseUrl = '';

    protected static string $token = '';
    protected static array $headers = [];

    /**
     * @param string $token
     */
    public static function setToken(string $token)
    {
        static::$token = $token;
    }

    /**
     * @param string $name
     * @param string $value
     */
    public static function addHeader(string $name, string $value)
    {
        static::$headers[$name] = $value;
    }

    /**
     * @return array
     */
    public static function getHeaders(): array
    {
        return static::$headers;
    }

    /**
     * @return string
     */
    public static function getToken(): string
    {
        if(static::$token) {
            return static::$token;
        }

        return env('OM_PMS_TOKEN', '');
    }

    /**
     * @param string $url
     */
    public static function setBaseUrl(string $url)
    {
        static::$baseUrl = $url;
    }

    /**
     * @return string
     */
    public static function getBaseUrl(): string
    {
        if(static::$baseUrl) {
            return static::$baseUrl;
        }

        return env('OM_PMS_BASE_URL', '');
    }

    /**
     * @param string $path
     * @return string
     */
    public static function getUrl(string $path = ''): string
    {
        return rtrim(static::getBaseUrl(), '/') . '/' . ltrim($path, '/');
    }

    /**
     * @param string $email
     * @param string $password
     * @param string $deviceName
     * @param string $scope
     * @return array
     * @throws \Exception
     */
    public static function getDeviceToken(string $email, string $password, string $deviceName, string $scope = 'admin'): array
    {
        $response = (new Factory)
            ->acceptJson()
            ->asJson()
            ->post(static::getUrl('auth/token/' . $scope), [
                'email' => $email,
                'password' => $password,
                'device_name' => $deviceName
            ]);

        if($response->successful()) {
            return $response->json();
        }

        throw new \Exception($response->json('message'));
    }

}
