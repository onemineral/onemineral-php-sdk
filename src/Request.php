<?php

namespace Onemineral\PMS\SDK;

use Illuminate\Http\Client\Factory;
use Illuminate\Support\Arr;
use Illuminate\Validation\ValidationException;

class Request
{

    /**
     * @param array $params
     * @param string $path
     * @param string $hydrateClass
     */
    public function __construct(public array $params, protected string $path, protected string $hydrateClass)
    {

    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function get(): mixed
    {
        return $this->execute();
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function execute(): mixed
    {
        $response = (new Factory)
            ->withToken(PMS::getToken())
            ->withHeaders(PMS::getHeaders())
            ->acceptJson()
            ->asJson()
            ->post(PMS::getUrl('rest/' . $this->path), $this->params);

        if($response->successful()) {
            return $this->hydrate($response->json(), $this->hydrateClass);
        }

        if($response->status() == 422) {
            throw ValidationException::withMessages($response->json('errors'));
        }

        throw new \Exception($response->json('message'));
    }

    /**
     * @param array  $results
     * @param string $class
     * @return array|\Illuminate\Support\Collection|mixed|null
     */
    public function hydrate(array $results, string $class)
    {
        if(isset($results[0])) {
            return collect($results)->map(fn($item) => new $class($item));
        } elseif (isset($results['data'])) {
            if(isset($results['total'])) {
                return new Paginator(
                    collect($results['data'])->map(fn($item) => new $class($item)),
                    $results['total'],
                    $results['per_page'],
                    $results['current_page']
                );
            } else {
                return collect($results['data'])->map(fn($item) => new $class($item));
            }
        } elseif($results) {
            return new $class($results);
        }

        return null;
    }

    /**
     * @param Conditions $conditions
     * @return $this
     */
    public function where(Conditions $conditions): static
    {
        $where = $this->params['where'] ?? [];
        unset($where['conditions'], $where['conditions_logic']);

        $where = array_merge($where, $conditions->toArray());

        $this->params['where'] = $where;

        return $this;
    }

    /**
     * @param Conditions $conditions
     * @return $this
     */
    public function whereAggregations(Conditions $conditions): static
    {
        $where = $this->params['where'] ?? [];

        unset($where['aggregate_conditions'], $where['aggregate_conditions_logic']);

        $where = array_merge($where, $conditions->toArray(true));

        $this->params['where'] = $where;

        return $this;
    }

    /**
     * @param array|string $with
     * @return $this
     */
    public function with(array|string $with): static
    {
        $with = Arr::wrap($with);
        $withParams = $this->params['with'] ?? [];

        $this->params['with'] = array_merge($withParams, $with);

        return $this;
    }

    /**
     * @param string $relation
     * @param Conditions|null $conditions
     * @param string|null $as
     * @return $this
     */
    public function withCount(string $relation, ?Conditions $conditions = null, ?string $as = null): Request
    {
        if( ! $as) {
            $as = $relation . '_count';
        }

        $this->withAggregation('count', $relation, $as, null, $conditions);

        return $this;
    }

    /**
     * @param string $relation
     * @param string $field
     * @param Conditions|null $conditions
     * @param string|null $as
     * @return Request
     */
    public function withSum(string $relation, string $field, ?Conditions $conditions = null, ?string $as = null): Request
    {
        if( ! $as) {
            $as = $field . '_sum';
        }

        $this->withAggregation('sum', $relation, $as, $field, $conditions);

        return $this;
    }

    /**
     * @param string $relation
     * @param string $field
     * @param Conditions|null $conditions
     * @param string|null $as
     * @return Request
     */
    public function withAvg(string $relation, string $field, ?Conditions $conditions = null, ?string $as = null): Request
    {
        if( ! $as) {
            $as = $field . '_avg';
        }

        $this->withAggregation('avg', $relation, $as, $field, $conditions);

        return $this;
    }

    /**
     * @param string $relation
     * @param string $field
     * @param Conditions|null $conditions
     * @param string|null $as
     * @return Request
     */
    public function withMax(string $relation, string $field, ?Conditions $conditions = null, ?string $as = null): Request
    {
        if( ! $as) {
            $as = $field . '_max';
        }

        $this->withAggregation('max', $relation, $as, $field, $conditions);

        return $this;
    }

    /**
     * @param string $relation
     * @param string $field
     * @param Conditions|null $conditions
     * @param string|null $as
     * @return Request
     */
    public function withMin(string $relation, string $field, ?Conditions $conditions = null, ?string $as = null): Request
    {
        if( ! $as) {
            $as = $relation . '_min';
        }

        $this->withAggregation('min', $relation, $as, $field, $conditions);

        return $this;
    }

    /**
     * @param string $type
     * @param string $relation
     * @param string $as
     * @param string|null $field
     * @param Conditions|null $conditions
     */
    protected function withAggregation(string $type, string $relation, string $as, ?string $field, ?Conditions $conditions)
    {
        $withAggregation = $this->params['with_aggregations'] ?? [];

        $aggregation = [
            'relation' => $relation,
            'as' => $as,
            'type' => $type
        ];

        if($field) {
            $aggregation['field'] = $field;
        }

        if($conditions) {
            $aggregation['where'] = $conditions->toArray();
        }

        $withAggregation[] = $aggregation;
        $this->params['with_aggregations'] = $withAggregation;
    }

    /**
     * @param int $page
     * @param int $perpage
     * @return $this
     */
    public function paginate(int $page, int $perpage = 30): static
    {
        $this->params['paginate'] = [
            'page' => $page,
            'perpage' => $perpage
        ];

        return $this;
    }

    /**
     * @param string $field
     * @param string $direction
     * @param string|null $locale
     * @return $this
     */
    public function orderBy(string $field, string $direction, ?string $locale = null): static
    {
        $sort = $this->params['sort'] ?? [];

        $currentSort = [
            'field' => $field,
            'direction' => $direction
        ];

        if($locale) {
            $currentSort['locale'] = $locale;
        }

        $sort[] = $currentSort;

        $this->params['sort'] = $sort;

        return $this;
    }

}