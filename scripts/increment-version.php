<?php

$version = @$argv[1];
$branch  = @$argv[2];

if(! ($branch && $version)) {
    echo 'Please provide the version and branch';
    exit(1);
}

[$version] = explode('-', $version);

$versionParts = explode('.', $version);

$versionParts[count($versionParts) - 1] += 1;

$version = implode('.', $versionParts);

if($branch !== 'main') {
    $version .= '-' . $branch;
}

echo $version;