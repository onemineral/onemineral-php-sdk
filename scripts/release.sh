#!/usr/bin/env bash

set -e

echo 'Checking git status'
# Make sure the working directory is clear.
if [[ ! -z "$(git status --porcelain)" ]]
then
    echo "Your working directory is dirty. Did you forget to commit your changes?"

    exit 1
fi

echo 'Run fetch origin'
# Make sure latest changes are fetched first.
git fetch origin

CURRENT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
CURRENT_TAG_VERSION=$(git describe --tags $(git rev-list --tags --max-count=1))
echo $CURRENT_TAG_VERSION
VERSION=$(php ./scripts/increment-version.php $CURRENT_TAG_VERSION $CURRENT_BRANCH)

echo $VERSION

echo 'Check if branch is up to date'
# Make sure that release branch is in sync with origin.
if [[ $(git rev-parse HEAD) != $(git rev-parse origin/$CURRENT_BRANCH) ]]
then
    echo "Your branch is out of date with its upstream. Did you forget to pull or push any changes before releasing?"

    exit 1
fi

echo 'Create tag'
# Tag Framework
git tag $VERSION

echo 'Push tag'
git push origin --tags
