#!/bin/sh

PROJECT=`php -r "echo dirname(dirname(realpath('$0')));"`

echo "Running unit tests..."
php $PROJECT/vendor/bin/phpunit

if [ $? != 0 ]
then
    echo "Fix the errors before commit."
    exit 1
fi

php $PROJECT/vendor/bin/security-checker security:check

if [ $? != 0 ]
then
    echo "Fix security issues before commit."
    exit 1
fi

exit $?
