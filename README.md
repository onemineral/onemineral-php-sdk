# Rentalwise PHP SDK

## Install


`composer require onemineral/php-sdk`


## Initialize the sdk


```php
use Onemineral\PMS\SDK\PMS;

PMS::setBaseURL('https://demo.rentalwise.io');
PMS::setToken('rentalwise-api-token');
```

## Query properties

```php
use Onemineral\PMS\SDK\PMS;
use Onemineral\PMS\SDK\Conditions;
use Onemineral\PMS\SDK\Paginator;

// Build conditions for property fields
$conditions = Conditions::factory()
    // query only enabled properties
    ->where('status', '=', 'enabled')
    // and where properties are in these location ids
    ->where('location', 'in', [1, 2, 3]);

/** @var Paginator $propertiesPaginator **/
$propertiesPaginator = PMS::property()->query()
    // apply conditions
    ->where($conditions);
    // paginate response
    ->paginate(1, 10)
    // include property location relation in response
    ->with(['location']);
    ->get();
```
