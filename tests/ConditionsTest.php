<?php

namespace Onemineral\PMS\SDK\Tests;

use Onemineral\PMS\SDK\Conditions;

class ConditionsTest extends TestCase
{

    public function testConditions()
    {
        $conditions = Conditions::factory()->where('field1', '=', 123)
            ->orWhere('field2', '>', 55)
            ->orWhere(
                Conditions::factory()->where('field3', '>=', 200)
                    ->where('field4', 'contains', ['value' => 'abc'])
            )
            ->toArray();

        $this->assertEquals('1 or 2 or (3 and 4)', $conditions['conditions_logic']);

        $this->assertEquals([
            [
                'field' => 'field1',
                'eq' => 123
            ],
            [
                'field' => 'field2',
                'gt' => 55
            ],
            [
                'field' => 'field3',
                'gte' => 200
            ],
            [
                'field' => 'field4',
                'contains' => [
                    'value' => 'abc'
                ]
            ]
        ], $conditions['conditions']);
    }

}